-- Disable PK triggers
alter trigger Department_PK_trig disable;
alter trigger ParentChild_PK_trig disable;
alter trigger Person_PK_trig disable;
alter trigger ProjEmpCurProj_PK_trig disable;
alter trigger Project_PK_trig disable;
alter trigger ProjEmpCurProj_PK_trig disable;

-- Break circular dependencies for FK constraints
alter table ParentChild disable constraint ParentChild_Person_FK;
alter table ParentChild disable constraint ParentChild_Person_FKv1;
alter table Person disable constraint Person_Department_FK;
alter table Person disable constraint Person_Person_FK;
alter table Person disable constraint Person_Person_FKv1;
alter table ProjEmpCurProj disable constraint ProjEmpCurProj_Person_FK;
alter table ProjEmpCurProj disable constraint ProjEmpCurProj_Project_FK;
alter table Project disable constraint Project_Department_FK;
alter table Project disable constraint Project_Person_FK;
alter table Project disable constraint Project_Project_FK;

-- Truncate tables
truncate table Department;
truncate table ParentChild;
truncate table Person;
truncate table ProjEmpCurProj;
truncate table Project;

-- Insert sample data
insert into Person (
    person_id,
    type,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    spouse
) values (
    1, -- person id
    'person', -- type
    'John', -- first_name
    'Doe', -- last_name
    78705, -- zipcode
    1234567890, -- home_phone
    1, -- us_citizen
    2 -- spouse
);

insert into Person (
    person_id,
    type,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen
) values (
    11, -- person id
    'person', -- type
    'Lily', -- first_name
    'Bane', -- last_name
    78705, -- zipcode
    1231234523, -- home_phone
    1 -- us_citizen
);

insert into manager_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    bonus,
    employee_manager,
    manager_dept
) values (
    12, -- person id
    'Helen', -- first_name
    'Reeds', -- last_name
    78705, -- zipcode
    1234295023, -- home_phone
    1, -- us_citizen
    202, -- employee id
    120000, -- salary
    0, -- salary exception
    10000, -- bonus
    8, -- employee_manager
    2 -- manager_dept
);

insert into manager_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    bonus,
    employee_manager,
    manager_dept
) values (
    3, -- person id
    'Bob', -- first_name
    'Bane', -- last_name
    78705, -- zipcode
    1231234523, -- home_phone
    1, -- us_citizen
    201, -- employee id
    120000, -- salary
    0, -- salary exception
    10000, -- bonus
    8, -- employee_manager
    1 -- manager_dept
);

insert into president_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    bonus
) values (
    8, -- person id
    'Bill', -- first_name
    'Gates', -- last_name
    23598, -- zipcode
    1231239825, -- home_phone
    1, -- us_citizen
    777, -- employee id
    400000, -- salary
    0, -- salary exception
    40000 -- bonus
);

insert into employee_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    spouse,
    employee_id,
    salary,
    salary_exception,
    employee_manager
) values (
    2, -- person id
    'Jane', -- first_name
    'Doe', -- last_name
    78705, -- zipcode
    1234567890, -- home_phone
    1, -- us_citizen
    1, -- spouse
    101, -- employee id
    90000, -- salary
    0, -- salary exception
    3 -- employee_manager
);

insert into employee_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    employee_manager
) values (
    4, -- person id
    'Sam', -- first_name
    'Pearson', -- last_name
    12345, -- zipcode
    9237513823, -- home_phone
    1, -- us_citizen
    102, -- employee id
    85000, -- salary
    1, -- salary exception
    3 -- employee_manager
);

insert into project_employee_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    employee_manager
) values (
    5, -- person id
    'Karen', -- first_name
    'Xu', -- last_name
    21345, -- zipcode
    9239283498, -- home_phone
    1, -- us_citizen
    103, -- employee id
    85000, -- salary
    0, -- salary exception
    3 -- employee_manager
);

insert into project_employee_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    employee_manager
) values (
    6, -- person id
    'Lydia', -- first_name
    'Wong', -- last_name
    21345, -- zipcode
    9231234566, -- home_phone
    0, -- us_citizen
    104, -- employee id
    95000, -- salary
    1, -- salary exception
    3 -- employee_manager
);

insert into project_employee_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    employee_manager
) values (
    7, -- person id
    'Dylan', -- first_name
    'Williams', -- last_name
    21345, -- zipcode
    9231239834, -- home_phone
    1, -- us_citizen
    105, -- employee id
    95000, -- salary
    0, -- salary exception
    12 -- employee_manager
);

insert into previous_employee_view (
    person_id,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    is_fired,
    previous_employee_salary
) values (
    10, -- person id
    'Emily', -- first_name
    'Smith', -- last_name
    21345, -- zipcode
    9287241234, -- home_phone
    0, -- us_citizen
    1, -- is_fired
    80000 -- salary
);

insert into Person (
    person_id,
    type,
    first_name,
    last_name,
    zipcode,
    home_phone,
    us_citizen,
    employee_id,
    salary,
    salary_exception,
    manager_dept,
    employee_manager
) values (
    9, -- person id
    'interim_manager',
    'Allison', -- first_name
    'Davis', -- last_name
    21345, -- zipcode
    9239823521, -- home_phone
    1, -- us_citizen
    301, -- employee id
    85000, -- salary
    0, -- salary exception
    2, -- manager_dept
    8 -- employee_manager
);

insert into Department (
    dept_no,
    dept_name
) values (
    1,
    'Sales'
);

insert into Department (
    dept_no,
    dept_name
) values (
    2,
    'Engineering'
);

insert into current_project_view (
    project_no,
    project_title,
    project_manager,
    dept_assigned,
    project_active
) values (
    1,
    'Code Blue',
    3,
    1,
    1
);

insert into current_project_view (
    project_no,
    project_title,
    project_manager,
    dept_assigned,
    project_active
) values (
    2,
    'Secret Web App',
    12,
    2,
    1
);

insert into current_project_view (
    project_no,
    project_title,
    project_manager,
    dept_assigned,
    subproject_of,
    project_active
) values (
    3,
    'Frontend',
    12,
    2,
    2,
    1
);

insert into current_project_view (
    project_no,
    project_title,
    project_manager,
    dept_assigned,
    subproject_of,
    project_active
) values (
    4,
    'Code Red',
    3,
    1,
    1,
    0
);

insert into previous_project_view (
    project_no,
    project_title,
    project_manager,
    dept_assigned,
    end_date
) values (
    5,
    'Backend',
    9,
    2,
    SYSDATE
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    1,
    5,
    1
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    2,
    6,
    1
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    3,
    7,
    2
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    4,
    9,
    2
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    5,
    7,
    3
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    6,
    5,
    4
);

insert into ProjEmpCurProj (
    pecp_id,
    person_id,
    project_no
) values (
    7,
    9,
    5
);

-- Reenable PK triggers
alter trigger Department_PK_trig enable;
alter trigger ParentChild_PK_trig enable;
alter trigger Person_PK_trig enable;
alter trigger ProjEmpCurProj_PK_trig enable;
alter trigger Project_PK_trig enable;
alter trigger ProjEmpCurProj_PK_trig enable;

-- Break circular dependencies for FK constraints
alter table ParentChild enable constraint ParentChild_Person_FK;
alter table ParentChild enable constraint ParentChild_Person_FKv1;
alter table Person enable constraint Person_Department_FK;
alter table Person enable constraint Person_Person_FK;
alter table Person enable constraint Person_Person_FKv1;
alter table ProjEmpCurProj enable constraint ProjEmpCurProj_Person_FK;
alter table ProjEmpCurProj enable constraint ProjEmpCurProj_Project_FK;
alter table Project enable constraint Project_Department_FK;
alter table Project enable constraint Project_Person_FK;
alter table Project enable constraint Project_Project_FK;
