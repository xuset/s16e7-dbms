#!/bin/sh

cat company.ddl | sed -e "s/;/;~/" -e 's/"//g' | tr -s "\n~" " \n" | grep "ALTER TABLE.*KEY" | tr -d '\15\32' | java -jar DDLParser.jar > company.ddl2

cat company.ddl | sed -e "s/;/;~/" -e 's/"//g' | tr -s "\n~" " \n" | grep "ALTER TABLE.*KEY" | tr -d '\15\32' | java -jar DDLParser2.jar > company.ddl3

cat company.ddl > super.ddl
cat company.ddl2 >> super.ddl
cat company.ddl3 >> super.ddl
cat company.ddl4 >> super.ddl

cat super.ddl
