-- Generated by Oracle SQL Developer Data Modeler 4.1.3.901
--   at:        2016-03-29 14:35:33 CDT
--   site:      Oracle Database 12c
--   type:      Oracle Database 12c




CREATE TABLE Department
  (
    dept_no   INTEGER NOT NULL ,
    dept_name VARCHAR2 (20) NOT NULL
  ) ;
ALTER TABLE Department ADD CONSTRAINT Department_PK PRIMARY KEY ( dept_no ) ;


CREATE TABLE ParentChild
  (
    parentchild_id INTEGER NOT NULL ,
    parent         INTEGER ,
    child          INTEGER
  ) ;
ALTER TABLE ParentChild ADD CONSTRAINT ParentChild_PK PRIMARY KEY ( parentchild_id ) ;


CREATE TABLE Person
  (
    person_id                INTEGER NOT NULL ,
    type                     VARCHAR2 (255) NOT NULL ,
    first_name               VARCHAR2 (255) NOT NULL ,
    last_name                VARCHAR2 (255) NOT NULL ,
    zipcode                  INTEGER ,
    home_phone               INTEGER ,
    us_citizen               NUMBER NOT NULL ,
    spouse                   INTEGER ,
    employee_id              INTEGER ,
    salary                   INTEGER ,
    salary_exception         NUMBER ,
    is_fired                 NUMBER ,
    previous_employee_salary INTEGER ,
    bonus                    INTEGER ,
    employee_manager         INTEGER ,
    manager_dept             INTEGER
  ) ;
CREATE UNIQUE INDEX Person__IDX ON Person
  (
    spouse ASC
  )
  ;
ALTER TABLE Person ADD CONSTRAINT Person_PK PRIMARY KEY ( person_id ) ;


CREATE TABLE ProjEmpCurProj
  (
    pecp_id    INTEGER NOT NULL ,
    person_id  INTEGER ,
    project_no INTEGER
  ) ;
ALTER TABLE ProjEmpCurProj ADD CONSTRAINT ProjEmpCurProj_PK PRIMARY KEY ( pecp_id ) ;


CREATE TABLE Project
  (
    project_no       INTEGER NOT NULL ,
    type             VARCHAR2 (255) NOT NULL ,
    project_title    VARCHAR2 (20) NOT NULL ,
    dept_assigned    INTEGER ,
    subproject_of    INTEGER ,
    project_manager  INTEGER ,
    project_active   NUMBER ,
    end_date         DATE ,
    est_person_hours INTEGER
  ) ;
ALTER TABLE Project ADD CONSTRAINT Project_PK PRIMARY KEY ( project_no ) ;


ALTER TABLE ParentChild ADD CONSTRAINT ParentChild_Person_FK FOREIGN KEY ( parent ) REFERENCES Person ( person_id ) ;

ALTER TABLE ParentChild ADD CONSTRAINT ParentChild_Person_FKv1 FOREIGN KEY ( child ) REFERENCES Person ( person_id ) ;

ALTER TABLE Person ADD CONSTRAINT Person_Department_FK FOREIGN KEY ( manager_dept ) REFERENCES Department ( dept_no ) ;

ALTER TABLE Person ADD CONSTRAINT Person_Person_FK FOREIGN KEY ( spouse ) REFERENCES Person ( person_id ) ;

ALTER TABLE Person ADD CONSTRAINT Person_Person_FKv1 FOREIGN KEY ( employee_manager ) REFERENCES Person ( person_id ) ;

ALTER TABLE ProjEmpCurProj ADD CONSTRAINT ProjEmpCurProj_Person_FK FOREIGN KEY ( person_id ) REFERENCES Person ( person_id ) ;

ALTER TABLE ProjEmpCurProj ADD CONSTRAINT ProjEmpCurProj_Project_FK FOREIGN KEY ( project_no ) REFERENCES Project ( project_no ) ;

ALTER TABLE Project ADD CONSTRAINT Project_Department_FK FOREIGN KEY ( dept_assigned ) REFERENCES Department ( dept_no ) ;

ALTER TABLE Project ADD CONSTRAINT Project_Person_FK FOREIGN KEY ( project_manager ) REFERENCES Person ( person_id ) ;

ALTER TABLE Project ADD CONSTRAINT Project_Project_FK FOREIGN KEY ( subproject_of ) REFERENCES Project ( project_no ) ;


-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             5
-- CREATE INDEX                             1
-- ALTER TABLE                             15
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- TSDP POLICY                              0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
