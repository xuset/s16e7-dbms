-- PL/SQL Code for Part 1

-- Page 4: Create Project
BEGIN
INSERT INTO current_project_view (project_active, project_title, project_manager, dept_assigned, subproject_of)
VALUES (0, :P4_PROJECT_TITLE, :P4_PROJECT_MANAGER, :P4_DEPT_ASSIGNED, :P4_SUBPROJECT_OF);

SELECT project_no INTO :P4_RETURNED_PROJECT_NO FROM current_project_view
WHERE (project_title = :P4_PROJECT_TITLE);
END;

-- Page 4: Insert Project Employees
DECLARE
    employee_list APEX_APPLICATION_GLOBAL.VC_ARR2;
    curr_employee_id VARCHAR2(50);

BEGIN

IF(:P4_EMPLOYEE_LIST IS NOT NULL) THEN

employee_list := APEX_UTIL.STRING_TO_TABLE(:P4_EMPLOYEE_LIST);

FOR i IN 1..employee_list.COUNT LOOP

    SELECT person_id INTO curr_employee_id FROM person
        WHERE (first_name || ' ' || last_name = employee_list(i) AND ROWNUM <= 1);
    
    UPDATE Person
    SET type='project_employee'
    WHERE person_id=curr_employee_id;

    INSERT INTO ProjEmpCurProj
    (person_id, project_no)
    VALUES 
    (curr_employee_id, :P4_RETURNED_PROJECT_NO);

END LOOP;

END IF;

END;

-- Page 18: Login
BEGIN

SELECT type INTO :P18_LOGIN_TYPE
FROM person
WHERE :P18_TXT_EMP_ID = employee_id;

IF :P18_LOGIN_TYPE = 'previous_employee' THEN
    :P18_LOGIN_TYPE := NULL;
END IF;

END;